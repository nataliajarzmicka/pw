﻿namespace LINQ
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.plikToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newEmployeeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salariesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.employeeListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pomocToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.searchBox = new System.Windows.Forms.TextBox();
            this.employeeList = new System.Windows.Forms.ListBox();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.fireEmployee = new System.Windows.Forms.Button();
            this.employeeTotalAmount = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.employeeSalary = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.employeeSex = new System.Windows.Forms.Label();
            this.employeeDateOfBirth = new System.Windows.Forms.Label();
            this.employeeLastName = new System.Windows.Forms.Label();
            this.employeeFirstName = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.statsMoneySpentCurrentYear = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.statsTotalMoneySpent = new System.Windows.Forms.Label();
            this.statsPaymentsCount = new System.Windows.Forms.Label();
            this.statsAvgSalary = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.refreshStats = new System.Windows.Forms.Button();
            this.statsSexRatio = new System.Windows.Forms.Label();
            this.statsAvgAge = new System.Windows.Forms.Label();
            this.statsNumOfEmployees = new System.Windows.Forms.Label();
            this.statsSexRatioLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.employeeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.employeeBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.plikToolStripMenuItem,
            this.pomocToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(803, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // plikToolStripMenuItem
            // 
            this.plikToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newEmployeeToolStripMenuItem,
            this.salariesToolStripMenuItem,
            this.employeeListToolStripMenuItem});
            this.plikToolStripMenuItem.Name = "plikToolStripMenuItem";
            this.plikToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.plikToolStripMenuItem.Text = "Menu";
            // 
            // newEmployeeToolStripMenuItem
            // 
            this.newEmployeeToolStripMenuItem.Name = "newEmployeeToolStripMenuItem";
            this.newEmployeeToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.newEmployeeToolStripMenuItem.Text = "New employee";
            this.newEmployeeToolStripMenuItem.Click += new System.EventHandler(this.newEmployeeToolStripMenuItem_Click);
            // 
            // salariesToolStripMenuItem
            // 
            this.salariesToolStripMenuItem.Name = "salariesToolStripMenuItem";
            this.salariesToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.salariesToolStripMenuItem.Text = "Salaries";
            this.salariesToolStripMenuItem.Click += new System.EventHandler(this.salariesToolStripMenuItem_Click);
            // 
            // employeeListToolStripMenuItem
            // 
            this.employeeListToolStripMenuItem.Name = "employeeListToolStripMenuItem";
            this.employeeListToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.employeeListToolStripMenuItem.Text = "Employee list";
            this.employeeListToolStripMenuItem.Click += new System.EventHandler(this.employeeListToolStripMenuItem_Click);
            // 
            // pomocToolStripMenuItem
            // 
            this.pomocToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showInfoToolStripMenuItem});
            this.pomocToolStripMenuItem.Name = "pomocToolStripMenuItem";
            this.pomocToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.pomocToolStripMenuItem.Text = "Help";
            // 
            // showInfoToolStripMenuItem
            // 
            this.showInfoToolStripMenuItem.Name = "showInfoToolStripMenuItem";
            this.showInfoToolStripMenuItem.Size = new System.Drawing.Size(127, 22);
            this.showInfoToolStripMenuItem.Text = "Show info";
            this.showInfoToolStripMenuItem.Click += new System.EventHandler(this.showInfoToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 411);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(803, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(106, 17);
            this.toolStripStatusLabel1.Text = "LINQ Example App";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 24);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer1.Size = new System.Drawing.Size(803, 387);
            this.splitContainer1.SplitterDistance = 266;
            this.splitContainer1.TabIndex = 2;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer2.IsSplitterFixed = true;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.searchBox);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.employeeList);
            this.splitContainer2.Size = new System.Drawing.Size(266, 387);
            this.splitContainer2.SplitterDistance = 25;
            this.splitContainer2.TabIndex = 0;
            // 
            // searchBox
            // 
            this.searchBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchBox.Location = new System.Drawing.Point(0, 0);
            this.searchBox.Name = "searchBox";
            this.searchBox.Size = new System.Drawing.Size(266, 20);
            this.searchBox.TabIndex = 0;
            // 
            // employeeList
            // 
            this.employeeList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.employeeList.FormattingEnabled = true;
            this.employeeList.Location = new System.Drawing.Point(0, 0);
            this.employeeList.Name = "employeeList";
            this.employeeList.Size = new System.Drawing.Size(266, 358);
            this.employeeList.TabIndex = 1;
            this.employeeList.SelectedIndexChanged += new System.EventHandler(this.employeeList_SelectedIndexChanged_1);
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.groupBox2);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.groupBox1);
            this.splitContainer3.Size = new System.Drawing.Size(533, 387);
            this.splitContainer3.SplitterDistance = 278;
            this.splitContainer3.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.fireEmployee);
            this.groupBox2.Controls.Add(this.employeeTotalAmount);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.employeeSalary);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.employeeSex);
            this.groupBox2.Controls.Add(this.employeeDateOfBirth);
            this.groupBox2.Controls.Add(this.employeeLastName);
            this.groupBox2.Controls.Add(this.employeeFirstName);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(533, 278);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Employee details";
            // 
            // fireEmployee
            // 
            this.fireEmployee.Location = new System.Drawing.Point(12, 186);
            this.fireEmployee.Name = "fireEmployee";
            this.fireEmployee.Size = new System.Drawing.Size(75, 23);
            this.fireEmployee.TabIndex = 14;
            this.fireEmployee.Text = "Fire";
            this.fireEmployee.UseVisualStyleBackColor = true;
            this.fireEmployee.Click += new System.EventHandler(this.fireEmployee_Click);
            // 
            // employeeTotalAmount
            // 
            this.employeeTotalAmount.AutoSize = true;
            this.employeeTotalAmount.Location = new System.Drawing.Point(146, 141);
            this.employeeTotalAmount.Name = "employeeTotalAmount";
            this.employeeTotalAmount.Size = new System.Drawing.Size(41, 13);
            this.employeeTotalAmount.TabIndex = 13;
            this.employeeTotalAmount.Text = "label14";
            this.employeeTotalAmount.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 141);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(108, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Total amount earned:";
            // 
            // employeeSalary
            // 
            this.employeeSalary.AutoSize = true;
            this.employeeSalary.Location = new System.Drawing.Point(146, 105);
            this.employeeSalary.Name = "employeeSalary";
            this.employeeSalary.Size = new System.Drawing.Size(41, 13);
            this.employeeSalary.TabIndex = 11;
            this.employeeSalary.Text = "label14";
            this.employeeSalary.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 105);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Salary:";
            // 
            // employeeSex
            // 
            this.employeeSex.AutoSize = true;
            this.employeeSex.Location = new System.Drawing.Point(146, 86);
            this.employeeSex.Name = "employeeSex";
            this.employeeSex.Size = new System.Drawing.Size(41, 13);
            this.employeeSex.TabIndex = 9;
            this.employeeSex.Text = "label14";
            this.employeeSex.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // employeeDateOfBirth
            // 
            this.employeeDateOfBirth.AutoSize = true;
            this.employeeDateOfBirth.Location = new System.Drawing.Point(146, 67);
            this.employeeDateOfBirth.Name = "employeeDateOfBirth";
            this.employeeDateOfBirth.Size = new System.Drawing.Size(41, 13);
            this.employeeDateOfBirth.TabIndex = 8;
            this.employeeDateOfBirth.Text = "label13";
            this.employeeDateOfBirth.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // employeeLastName
            // 
            this.employeeLastName.AutoSize = true;
            this.employeeLastName.Location = new System.Drawing.Point(146, 48);
            this.employeeLastName.Name = "employeeLastName";
            this.employeeLastName.Size = new System.Drawing.Size(41, 13);
            this.employeeLastName.TabIndex = 7;
            this.employeeLastName.Text = "label12";
            this.employeeLastName.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // employeeFirstName
            // 
            this.employeeFirstName.AutoSize = true;
            this.employeeFirstName.Location = new System.Drawing.Point(146, 29);
            this.employeeFirstName.Name = "employeeFirstName";
            this.employeeFirstName.Size = new System.Drawing.Size(41, 13);
            this.employeeFirstName.TabIndex = 6;
            this.employeeFirstName.Text = "label11";
            this.employeeFirstName.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 86);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(28, 13);
            this.label10.TabIndex = 3;
            this.label10.Text = "Sex:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 67);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(68, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Date of birth:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 48);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Last name:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 29);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(58, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "First name:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.statsMoneySpentCurrentYear);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.statsTotalMoneySpent);
            this.groupBox1.Controls.Add(this.statsPaymentsCount);
            this.groupBox1.Controls.Add(this.statsAvgSalary);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.refreshStats);
            this.groupBox1.Controls.Add(this.statsSexRatio);
            this.groupBox1.Controls.Add(this.statsAvgAge);
            this.groupBox1.Controls.Add(this.statsNumOfEmployees);
            this.groupBox1.Controls.Add(this.statsSexRatioLabel);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(533, 105);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Statistics";
            // 
            // statsMoneySpentCurrentYear
            // 
            this.statsMoneySpentCurrentYear.AutoSize = true;
            this.statsMoneySpentCurrentYear.Location = new System.Drawing.Point(152, 79);
            this.statsMoneySpentCurrentYear.Name = "statsMoneySpentCurrentYear";
            this.statsMoneySpentCurrentYear.Size = new System.Drawing.Size(35, 13);
            this.statsMoneySpentCurrentYear.TabIndex = 14;
            this.statsMoneySpentCurrentYear.Text = "label6";
            this.statsMoneySpentCurrentYear.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(7, 79);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(136, 13);
            this.label15.TabIndex = 13;
            this.label15.Text = "Money spent (current year):";
            // 
            // statsTotalMoneySpent
            // 
            this.statsTotalMoneySpent.AutoSize = true;
            this.statsTotalMoneySpent.Location = new System.Drawing.Point(350, 60);
            this.statsTotalMoneySpent.Name = "statsTotalMoneySpent";
            this.statsTotalMoneySpent.Size = new System.Drawing.Size(35, 13);
            this.statsTotalMoneySpent.TabIndex = 12;
            this.statsTotalMoneySpent.Text = "label6";
            this.statsTotalMoneySpent.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // statsPaymentsCount
            // 
            this.statsPaymentsCount.AutoSize = true;
            this.statsPaymentsCount.Location = new System.Drawing.Point(350, 40);
            this.statsPaymentsCount.Name = "statsPaymentsCount";
            this.statsPaymentsCount.Size = new System.Drawing.Size(35, 13);
            this.statsPaymentsCount.TabIndex = 11;
            this.statsPaymentsCount.Text = "label5";
            this.statsPaymentsCount.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // statsAvgSalary
            // 
            this.statsAvgSalary.AutoSize = true;
            this.statsAvgSalary.Location = new System.Drawing.Point(350, 20);
            this.statsAvgSalary.Name = "statsAvgSalary";
            this.statsAvgSalary.Size = new System.Drawing.Size(35, 13);
            this.statsAvgSalary.TabIndex = 10;
            this.statsAvgSalary.Text = "label4";
            this.statsAvgSalary.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(205, 60);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(97, 13);
            this.label12.TabIndex = 9;
            this.label12.Text = "Total money spent:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(205, 40);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(86, 13);
            this.label13.TabIndex = 8;
            this.label13.Text = "Payments count:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(205, 20);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(80, 13);
            this.label14.TabIndex = 7;
            this.label14.Text = "Average salary:";
            // 
            // refreshStats
            // 
            this.refreshStats.Dock = System.Windows.Forms.DockStyle.Right;
            this.refreshStats.Location = new System.Drawing.Point(455, 16);
            this.refreshStats.Name = "refreshStats";
            this.refreshStats.Size = new System.Drawing.Size(75, 86);
            this.refreshStats.TabIndex = 6;
            this.refreshStats.Text = "Refresh";
            this.refreshStats.UseVisualStyleBackColor = true;
            this.refreshStats.Click += new System.EventHandler(this.refreshStats_Click);
            // 
            // statsSexRatio
            // 
            this.statsSexRatio.AutoSize = true;
            this.statsSexRatio.Location = new System.Drawing.Point(152, 60);
            this.statsSexRatio.Name = "statsSexRatio";
            this.statsSexRatio.Size = new System.Drawing.Size(35, 13);
            this.statsSexRatio.TabIndex = 5;
            this.statsSexRatio.Text = "label6";
            this.statsSexRatio.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // statsAvgAge
            // 
            this.statsAvgAge.AutoSize = true;
            this.statsAvgAge.Location = new System.Drawing.Point(152, 40);
            this.statsAvgAge.Name = "statsAvgAge";
            this.statsAvgAge.Size = new System.Drawing.Size(35, 13);
            this.statsAvgAge.TabIndex = 4;
            this.statsAvgAge.Text = "label5";
            this.statsAvgAge.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // statsNumOfEmployees
            // 
            this.statsNumOfEmployees.AutoSize = true;
            this.statsNumOfEmployees.Location = new System.Drawing.Point(152, 20);
            this.statsNumOfEmployees.Name = "statsNumOfEmployees";
            this.statsNumOfEmployees.Size = new System.Drawing.Size(35, 13);
            this.statsNumOfEmployees.TabIndex = 3;
            this.statsNumOfEmployees.Text = "label4";
            this.statsNumOfEmployees.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // statsSexRatioLabel
            // 
            this.statsSexRatioLabel.AutoSize = true;
            this.statsSexRatioLabel.Location = new System.Drawing.Point(7, 60);
            this.statsSexRatioLabel.Name = "statsSexRatioLabel";
            this.statsSexRatioLabel.Size = new System.Drawing.Size(67, 13);
            this.statsSexRatioLabel.TabIndex = 2;
            this.statsSexRatioLabel.Text = "Female ratio:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Average age: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Number of employees:";
            // 
            // employeeBindingSource
            // 
            this.employeeBindingSource.DataSource = typeof(LINQ.Employee);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(803, 433);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(800, 420);
            this.Name = "Form1";
            this.Text = "LINQ Example App";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.employeeBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem plikToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pomocToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ToolStripMenuItem newEmployeeToolStripMenuItem;
        private System.Windows.Forms.BindingSource employeeBindingSource;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.TextBox searchBox;
        private System.Windows.Forms.ListBox employeeList;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label statsSexRatio;
        private System.Windows.Forms.Label statsAvgAge;
        private System.Windows.Forms.Label statsNumOfEmployees;
        private System.Windows.Forms.Label statsSexRatioLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label employeeSex;
        private System.Windows.Forms.Label employeeDateOfBirth;
        private System.Windows.Forms.Label employeeLastName;
        private System.Windows.Forms.Label employeeFirstName;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ToolStripMenuItem showInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salariesToolStripMenuItem;
        private System.Windows.Forms.Label employeeSalary;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button refreshStats;
        private System.Windows.Forms.Label employeeTotalAmount;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label statsTotalMoneySpent;
        private System.Windows.Forms.Label statsPaymentsCount;
        private System.Windows.Forms.Label statsAvgSalary;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button fireEmployee;
        private System.Windows.Forms.ToolStripMenuItem employeeListToolStripMenuItem;
        private System.Windows.Forms.Label statsMoneySpentCurrentYear;
        private System.Windows.Forms.Label label15;
    }
}

