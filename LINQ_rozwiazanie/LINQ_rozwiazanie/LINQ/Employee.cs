﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ
{
    class Employee
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string sex { get; set; }
        public DateTime dateOfBirth { get; set; }
        public int salary { get; set; }

        public Employee(string first, string last, string sex, DateTime birth, int salary)
        {
            firstName = first;
            lastName = last;
            this.sex = sex;
            dateOfBirth = birth;
            this.salary = salary;
        }

        public override string ToString()
        {
            return firstName + " " + lastName;
        }
    }
}
