﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LINQ
{
    public partial class Form1 : Form
    {
        BindingList<Employee> employees = new BindingList<Employee>();
        BindingList<Payment> paymentList = new BindingList<Payment>();

        public Form1()
        {
            InitializeComponent();

            employees.Add(new Employee("Mariusz", "Zdancewicz", "male", new DateTime(1997, 7, 26), 10000));
            employees.Add(new Employee("Robert", "Kuźba", "male", new DateTime(1994, 1, 1), 1500));
            employees.Add(new Employee("Anna", "Kowalska", "female", new DateTime(1998, 3, 5), 3500));

            employeeList.DataSource = employees;
            updateStats();
        }

        private void newEmployeeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewEmployeeForm form = new NewEmployeeForm(employees);
            form.ShowDialog();
        }

        private void employeeList_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            Employee employee = (Employee)employeeList.SelectedItem;
            if (employee != null)
            {
                employeeFirstName.Text = employee.firstName;
                employeeLastName.Text = employee.lastName;
                employeeDateOfBirth.Text = employee.dateOfBirth.ToString("yyyy-MM-dd");
                employeeSex.Text = employee.sex;
                employeeSalary.Text = "" + employee.salary;

                int totalAmount = paymentList
                    .Where(payment => payment.employee == employee)
                    .Sum(payment => payment.amount);

                employeeTotalAmount.Text = "" + totalAmount;
            }
            
        }

        public void updateStats()
        {
            statsNumOfEmployees.Text = "" + employees.Count();
            statsAvgAge.Text = (employees.Count() > 0)? (employees.Average(employee => (DateTime.Now - employee.dateOfBirth).TotalDays) / 365).ToString("0.00") : "0";
            double femaleCount = employees.Count(employee => employee.sex == "female");
            statsSexRatio.Text = (femaleCount / employees.Count()).ToString("0.00");
            statsPaymentsCount.Text = "" + paymentList.Count();
            statsTotalMoneySpent.Text = "" + paymentList.Sum(payment => payment.amount);
            statsAvgSalary.Text = (employees.Count() > 0) ? "" + employees.Average(employee => employee.salary) : "0";
            statsMoneySpentCurrentYear.Text = "" + paymentList.Where(payment => payment.date.Year == DateTime.Now.Year).Sum(payment => payment.amount);
        }

        private void showInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Info form = new Info();
            form.ShowDialog();
        }

        private void salariesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SalariesForm form = new SalariesForm(employees, paymentList);
            form.ShowDialog();
        }

        private void refreshStats_Click(object sender, EventArgs e)
        {
            updateStats();
        }

        private void fireEmployee_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Are you sure?", "Confirm", MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                employees.Remove((Employee)employeeList.SelectedItem);
                updateStats();
            }
        }

        private void employeeListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EmployeeList form = new EmployeeList(employees);
            form.ShowDialog();
        }
    }
}
