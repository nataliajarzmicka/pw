﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LINQ
{
    public partial class EmployeeList : Form
    {
        BindingList<Employee> employeeList;

        public EmployeeList(IBindingList list)
        {
            employeeList = (BindingList<Employee>)list;
            InitializeComponent();

            listBox1.DataSource = employeeList.OrderBy(employee => employee.lastName).OrderBy(employee => employee.firstName).ToList();
        }
    }
}
