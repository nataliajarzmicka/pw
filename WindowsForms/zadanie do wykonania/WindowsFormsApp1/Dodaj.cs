﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Dodaj : Form
    {

        public static List<Filmy> Films = new List<Filmy>();

        public Dodaj()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e) //dodanie filmow
        {
                    this.Close();
        }

        private void button2_Click(object sender, EventArgs e) //anulowanie
        {
            DialogResult dialogResult = MessageBox.Show("Czy na pewno chcesz anulować dodawanie nowego filmu?", "Anulować?", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                this.Close();
            }
        }
    }
}
