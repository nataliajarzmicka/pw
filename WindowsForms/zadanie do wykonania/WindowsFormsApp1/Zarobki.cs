﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Zarobki : Form
    {
        public Zarobki()
        {
            InitializeComponent();
        }

        private void checkIfZero() //sprawdzenie czy na pasku stanu jest ustawiona wartość maksymalna lub wartosc celu jest pusta 
        {
            if (progressBar1.Maximum == 0 || Cel.goal == 0)
            {
                Cel cel = new Cel();
                cel.ShowDialog();
            }
        }

        private void Zarobki_Load(object sender, EventArgs e)
        {
            checkIfZero();
            int money = Cel.goal; ;
            label3.Text = (KupnoBiletow.allMoney.ToString() + " złotych"); //ustawiony tekst label ktory przybiera wartosc zmiennej pobranej z forma kupno biletow ktory jest zamieniony na string
            progressBar1.Maximum = money; 

            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
