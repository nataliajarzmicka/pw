﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Cel : Form
    {
        public static int goal;
        public Cel()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e) //pobieranie wyniku okna dialogowego
        {   

            DialogResult dialogResult = MessageBox.Show("Wprowadzony cel pieniężny na dzisiaj to: "+ this.textBox1.Text +". Zatwierdzić?", "Cel pieniężny", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if(dialogResult == DialogResult.Yes)
            {
                Int32.TryParse(textBox1.Text, out goal);
                this.Close();
            } 
        }
    }
}
