﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Dodaj : Form
    {

        public static List<Filmy> Films = new List<Filmy>();

        public Dodaj()
        {
            InitializeComponent();
        }

        private bool CheckData() //sprawdzenie poprawnosci danych
        {
            int result;
            if (int.TryParse(textBox2.Text, out result)&& int.TryParse(textBox3.Text, out result) && !textBox4.Text.All(c => char.IsDigit(c)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void button1_Click(object sender, EventArgs e) //dodanie filmow
        {
            if (this.textBox1.Text != "" && this.textBox2.Text != "" && this.textBox3.Text != "" && this.textBox4.Text != "")
            {
                
                if(CheckData()==true)
                {

                    Films.Add(new Filmy(this.textBox1.Text, this.textBox2.Text, this.textBox3.Text, this.textBox4.Text));
                    MessageBox.Show("Film został dodany");
                    textBox1.Clear();
                    textBox2.Clear();
                    textBox3.Clear();
                    textBox4.Clear();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Wpisano błędne dane", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show("Proszę wprowadzić wszystkie dane", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void button2_Click(object sender, EventArgs e) //anulowanie
        {
            DialogResult dialogResult = MessageBox.Show("Czy na pewno chcesz anulować dodawanie nowego filmu?", "Anulować?", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                this.Close();
            }
        }
    }
}
