﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class DodajUsun : Form
    {
        public static List<Filmy> Films = Dodaj.Films;

        public DodajUsun()
        {
            InitializeComponent();
        }

        private void button1_Click_1(object sender, EventArgs e) //dodaj
        {
            Dodaj dodaj = new Dodaj(); 
            dodaj.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e) //usun
        {
            Usun usun = new Usun();
            if(Films.Count == 0)
            {
                MessageBox.Show("W bazie nie ma jeszcze żadnych filmów. Dodaj jakiś film.", "Problem", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                usun.ShowDialog();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
