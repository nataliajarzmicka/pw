﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Sprzedaz : Form
    {      
        public List<Klient> Klienci = KupnoBiletow.Klienci;
        public List<Filmy> Films = Dodaj.Films;
        public List<Bilet> Bilety = KupnoBiletow.Bilety;
        public string[,] Kupno = new string[30,30];
        private int index = 0;

        public Sprzedaz()
        {
            InitializeComponent();
        }

        private void getData() //wprowadza dane
        {
            for (int i = 1; i <= Klienci.Count(); i++)
            {
                index = 0;
                foreach (var klienci in Klienci) //wpisuje klientow
                {
                    Kupno[index, 1] = klienci.name;
                    Kupno[index, 2] = klienci.surname;
                    Kupno[index, 4] = klienci.price.ToString();
                    index++;
                }
                index = 0;
                foreach (var bilet in Bilety) // wpisuje bilety
                {
                    Kupno[index, 3] = bilet.ticketType;
                    Kupno[index, 5] = bilet.movie;
                    index++;
                }
            }
        }

        private void Form4_Load(object sender, EventArgs e) //wyswietlenie sprzadanych biletow
        {
            listView1.Items.Clear();
            getData();

            for (int i = 0; i <= index; i++)
            {
                    var row = new ListViewItem(new String[] { Kupno[i, 1], Kupno[i, 2], Kupno[i, 3], Kupno[i, 4], Kupno[i, 5] } );
                    listView1.Items.Add(row);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
 }
