/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package youtubevideoplayer;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

public class YouTubeVideoPlayer extends Application {
    
    @Override
    public void start(Stage primaryStage) {
        WebView webView =new WebView();
        webView.getEngine().load("https://www.youtube.com/watch?v=5yFfCAaedgA");
        webView.setPrefSize(840, 690);
        primaryStage.setTitle("Natalia Jarzmicka 38563");
        primaryStage.setScene(new Scene(webView));

        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
