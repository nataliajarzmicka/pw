import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class Dodaj_form extends JFrame{
    public JTextField textField1;
    public JPanel panel;
    private JTextField textField2;
    private JTextField textField3;
    private JComboBox comboBox1;
    private JButton dodajButton;

    public static ArrayList<Klient> uczniowie = Menu.uczniowie;

    public Dodaj_form() {
        dodajButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String imie = textField1.getText();
                String nazwisko = textField2.getText();
                String numer = textField3.getText();
                if(!imie.isEmpty() && !nazwisko.isEmpty() && !numer.isEmpty()){

                    Klient newuczen = new Klient(imie, nazwisko,  numer);
                    if(sprawdz(numer) == 1){

                        uczniowie.add(newuczen);

                        textField1.setText("");
                        textField2.setText("");
                        textField3.setText("");
                        JOptionPane.showMessageDialog(null, "Dodales nowego ucznia!");
                    }else{
                        JOptionPane.showMessageDialog(null, "Uczen o takim albumie istnieje!");
                    }
                }else{
                    JOptionPane.showMessageDialog(null, "Uzupelnij dane!");
                }
            }
        });
    }

    public int sprawdz(String album){

        ArrayList<Klient> uczniowie = Menu.uczniowie;
        for (Klient uczen:uczniowie){
            if(uczen.numer.equals(album)){
                return 0;

            }

        }
        return 1;
    }

    public ArrayList<Klient> returnarray()
    {
        return uczniowie;
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
