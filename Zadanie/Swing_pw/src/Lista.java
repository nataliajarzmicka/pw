import javax.swing.*;
import java.awt.*;

public class Lista extends JFrame {

    public Lista() {
        JFrame frame = new JFrame("Lista");
        frame.setContentPane(new Lista_form().getRootPane());

        frame.setPreferredSize(new Dimension(500, 500));
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}