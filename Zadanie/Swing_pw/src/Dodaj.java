import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class Dodaj extends JFrame {

    ArrayList<Klient> uczniowie = new ArrayList<Klient>();
    public Dodaj() {
        JFrame frame = new JFrame("Test");
        frame.setContentPane(new Dodaj_form().panel);

        frame.setPreferredSize(new Dimension(500, 500));
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}