﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Struktury_i_Klasy_generyczne
{
    class Klient
    {
        public int id { get; set; }
        public string name { get; set; }
        public string surname { get; set; }

        public Klient()
        {
        }

        public Klient(int id, string name, string surname)
        {
            this.id = id;
            this.name = name;
            this.surname = surname;
        }

        public void Wyswietl()
        {
            Console.WriteLine("ID: "+ id + "Imię: " + name + " Nazwisko: " + surname);
        }
    }
}
